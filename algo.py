import networkx as nx
from query_interface import get_exact_label_match, \
                                          get_all_property_relations_by_instance, \
                                          get_all_instance_properties
import re
import itertools
import logging
from tqdm import tqdm
import pprint
import networkx as nx
import matplotlib.pyplot as plt
from sumproduct import Variable, Factor, FactorGraph
import numpy as np
import math
from whoosh.analysis import NgramAnalyzer
from whoosh.fields import Schema, NGRAMWORDS, TEXT
from whoosh.qparser import QueryParser
import os, os.path
from whoosh import index
from whoosh.filedb.filestore import RamStorage
from operator import itemgetter

class Entity(object):
    def __init__(self, iid, itype, uri):
        self.id = iid
        self.type = itype
        self.uri = uri
        self.values = set()
    
    def add_value(self, value):
        self.values.add(value)

def get_common_cases(val):
    """ For a given value create the possible variations
        to search the ontology
    """
    cases = list()
    cases.append(val.upper())
    cases.append(val.lower())
    cases.append(val.title())
    return cases

def formatURI(uri, etype):
    """ Format the URI to a friendly version
    """ 
    if etype == "class":
        return "dbo:" + uri.replace("http://dbpedia.org/ontology/", "")
    elif etype == "property":
        return "dbp:" + uri.replace("http://dbpedia.org/ontology/", "")
    elif etype == "instance":
        return "dbr:" + uri.replace("http://dbpedia.org/resource/", "")
    return uri

def create_entity(entities, graph, candidate, etype):
    iid = formatURI(candidate, etype)
    if iid not in entities:
        entities[iid] = Entity(iid, etype, candidate)
        graph.add_node(iid)
    return iid

def delete_entity(entities, graph, iid):
    if iid in entities:
        entities.pop(iid)
        graph.remove_node(iid)

def get_cell_instance_classes(table, graph, entities):
    """ Function to find the possible candidates for each NE cell
        and corresponding class types of each cell candidate
    """
    for col in range(len(table.columns)):
        column = table.columns[col]

        col_id = create_entity(entities, graph, "C_%d"%col, "column")

        if not column.is_NE():
            continue

        for cell in tqdm(column.cells):
            #make sure that non NE cells are not here
            try:
                #TODO: find what's ging on here
                cell.value.split()
            except:
                continue

            value = " ".join(cell.value.decode("ascii", "ignore").split()).strip()
            value_perms =  get_common_cases(value)

            for perm in value_perms:
                candidates = get_exact_label_match(perm)

                if candidates is not None and len(candidates) > 0:
                    cell_candidate = None

                    for candidate in candidates:
                        column_candidate = candidate["type"]["value"]

                        if column_candidate.find("http://dbpedia.org/ontology") == -1:
                            continue
                        
                        if cell_candidate is None:
                            cell_candidate = candidate["uri"]["value"]
                            cell_id = create_entity(entities, graph, cell_candidate, "instance")
                            cell.candidates.add(cell_id)
                            graph.add_edge(col_id, cell_id)
                            graph.add_edge(cell_id, col_id)
                            break
                    break

def get_instance_properties(entities, graph, instance):
    #outbound properties
    properties = get_all_instance_properties(entities[instance].uri)

    if properties is not None:
        for p in properties:
            prop = p["property"]["value"]

            if prop.find("http://dbpedia.org/ontology") == -1:
                continue
            
            exclusion = [
                "http://dbpedia.org/ontology/abstract",
                "http://dbpedia.org/ontology/thumbnail",
                "http://dbpedia.org/ontology/wikiPageID",
                "http://dbpedia.org/ontology/wikiPageRevisionID",
                "http://dbpedia.org/ontology/wikiPageExternalLink"
            ]

            if prop in exclusion:
                continue
            
            prop_id = create_entity(entities, graph, prop, "property")
            graph.add_edge(instance, prop_id)
            # print instance, prop_id
            #adding the property labels
            entities[prop_id].add_value(p["callret-1"]["value"])
    
    #this one find the properties that are candidates for the column
    #finds column property candidate space
    #we append these to the graph but leave as it is to check
    #if a class would link to them as their own properties
    properties = get_all_property_relations_by_instance(entities[instance].uri)
    if properties is not None:
        for p in properties:
            prop = p["property"]["value"]

            if prop.find("http://dbpedia.org/ontology") == -1:
                continue

            exclusion = [
                "http://dbpedia.org/ontology/wikiPageRedirects",
                "http://dbpedia.org/ontology/wikiPageDisambiguates"
            ]

            if prop in exclusion:
                continue
            
            prop_id = create_entity(entities, graph, prop, "property")
            graph.add_edge(prop_id, instance)
            # print prop_id, instance
            #adding the property labels
            entities[prop_id].add_value(p["callret-1"]["value"])

def get_header_properties(entities, graph, table):
    for col in tqdm(range(len(table.columns))):
        column = table.columns[col]

        col_id = create_entity(entities, graph, "C_%d"%col, "column")
        headers = column.header.split()

        for header in headers:
            value = header.strip().lower()
            candidates = get_exact_label_match(value)

            if candidates is not None and len(candidates) > 0:
                for candidate in candidates:
                    property_candidate = candidate["uri"]["value"]

                    if property_candidate.find("http://dbpedia.org/ontology") == -1:
                        continue
                    
                    prop_id = create_entity(entities, graph, property_candidate, "property")
                    graph.add_edge(col_id, prop_id)

def search_column_headers(entities, graph, table):
    #initiallize the Bigram index
    schema = Schema(
        title=NGRAMWORDS(minsize=2, maxsize=4, stored=True, field_boost=1.0, 
                         tokenizer=None, at='start', queryor=False, sortable=False),
        uri=TEXT(stored=True)
    )

    storage = RamStorage()
    ix = storage.create_index(schema)
    writer = ix.writer()
    for e in entities:
        entity = entities[e]
        if entity.type != "property":
            continue
        
        for value in entity.values:
            writer.add_document(title=unicode(value), uri=unicode(e))
    
    writer.commit()

    #loop the literal colunm headers
    for c in range(len(table.columns)):
        column = table.columns[c]
        # if column.is_NE():
        #     continue
        
        query = column.header
        col_id = "C_%d" % c
        qp = QueryParser("title", schema=ix.schema)

        with ix.searcher() as searcher:
            for word in query.split():
                q = qp.parse(word.strip())
                results = searcher.search(q)
                for result in results:
                    graph.add_edge(result['uri'], col_id)

def visualize_graph(graph, entities):
    mapping = {
        "class": "blue",
        "instance": "green",
        "col_property": "red",
        "clz_property": "yellow",
        "property": "pink",
        "column": "red"
    }
    node_color = map(lambda node: mapping[entities[node].type],graph.nodes())
    nx.draw(graph, with_labels=True, node_color=node_color)
    plt.show()

def process_properties(graph, entities):
    connected = []
    blocked = []
    for node in graph.nodes():
        if entities[node].type == "property":
            if graph.in_degree(node) > 0 and graph.out_degree(node) > 0:
                connected.append(node)
            else:
                blocked.append(node)
    return connected, blocked

def connect_columns(graph, entities, table):
    edges = list()

    for c1 in range(len(table.columns)):
        c1_id = "C_%d" % c1
        for c2 in range(len(table.columns)):
            c2_id = "C_%d" % c2

            if nx.has_path(graph, c1_id, c2_id):
                for path in nx.algorithms.all_simple_paths(graph, c1_id, c2_id, 4):
                    if len(path) == 5:
                        edges.append([c1_id, path[2]])
                        edges.append([path[2], c2_id])
                    if len(path) == 4:
                        edges.append([c1_id, path[2]])
                        edges.append([path[2], c2_id])
    
    for edge in edges:
        n1 = edge[0]
        n2 = edge[1]
        if graph.has_edge(n1, n2):
            if 'weight' not in graph[n1][n2]:
                graph[n1][n2]['weight'] = 1    
            graph[n1][n2]['weight'] += 1
        else:
            graph.add_edge(n1, n2, weight=1)

def algo(table):
    EARLY_STOP_THRESHOLD = 5

    #graph data structure to hold the instance, class and property candidates
    graph = nx.DiGraph()

    #entities hold all the entity instances added to the graph
    entities = dict()

    #get cell instance candidates from ontology along with the belonging classes
    #these will be added to entities and the graph as well
    get_cell_instance_classes(table, graph, entities)

    #loop the rows
    early_stopping_started = False
    last_connected_set = -1
    early_stopping_count = 0
    total_iterations = 0

    for row in tqdm(table.rows):
        total_iterations += 1
        for cell in row.cells:
            if not cell.is_NE():
                continue
            
            # print cell.value

            for candidate in cell.candidates:
                get_instance_properties(entities, graph, candidate)

        conn, bloc = process_properties(graph, entities)
        if last_connected_set == -1:
            last_connected_set = len(conn)
        else:
            current_connected_set = len(conn)

            if current_connected_set == last_connected_set:
                if not early_stopping_started:
                    early_stopping_started = True
                early_stopping_count += 1
            else:
                early_stopping_count = 0
                early_stopping_started = False
            
            last_connected_set = current_connected_set
        
        if early_stopping_count >= EARLY_STOP_THRESHOLD:
            break


    # get_header_properties(entities, graph, table)
    search_column_headers(entities, graph, table)

    # visualize_graph(graph, entities)

    conn, bloc = process_properties(graph, entities)
    for node in bloc:
        delete_entity(entities, graph, node)

    logging.info("Found %d connected properties in %d/%d iterations" % (len(conn), total_iterations,
                                                                        len(table.rows)))
    # visualize_graph(graph, entities)                                  
    connect_columns(graph, entities, table)

    for node in graph.nodes():
        if entities[node].type == "instance":
            delete_entity(entities, graph, node)

    #get weighted properties for each pair of columns
    output = []
    for c1 in range(len(table.columns)):
        c1_id = "C_%d" % c1
        for c2 in range(len(table.columns)):
            c2_id = "C_%d" % c2
            if c1 == c2:
                continue
            if nx.has_path(graph, c1_id, c2_id):
                for path in nx.algorithms.all_simple_paths(graph, c1_id, c2_id, cutoff=3):    
                    if len(path) == 3:
                        weight = min(graph[c1_id][path[1]]['weight'], 
                                     graph[path[1]][c2_id]['weight'])
                        output.append([c1, c2, path[1], weight])
    
    # visualize_graph(graph, entities)

    #sort the outputs
    output = sorted(output, key=itemgetter(3), reverse=True)
    return output
    