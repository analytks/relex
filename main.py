"""T2D evaluator"""

from os import walk
from os.path import isfile, join, exists
import logging
from table import Table
import csv
import coloredlogs

from algo import algo

coloredlogs.install()

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("evaluator-t2d")

def evaluate(data_dir):
    """ Function to evaluate T2D compelete dataset
        Args:
            data_dir        directory where the source tables exist
    """

    if not exists(data_dir):
        logger.error("Data directory: %s does not exist")
        return None

    logger.info("Reading data set")
    (_, _, filenames) = walk(data_dir).next()
    current = 0
    for table_file in filenames:
        current += 1
        logger.info("Progress : %d/%d" % (current, len(filenames)) )
        logger.info("Annotating : %s" % table_file)

        
        #parse the source table
        try:
            tb = Table()
            tb.parse_csv(join(data_dir, table_file))

            output = algo(tb)
            
            mapping = {}
            for o in output:
                if o[0] not in mapping:
                    mapping[o[0]] = dict()
                if o[1] not in mapping[o[0]]:
                    mapping[o[0]][o[1]] = [o[2], o[3]]
            
            print mapping
            print output

        except Exception as e:
            logger.error("Error parsing table: %s" % table_file)
            logger.exception("Issue")


if __name__ == "__main__":
    evaluate("./dataset/")


        
